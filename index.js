var express = require('express')

var app = express()

var db = require('./db.js');

var bodyParser=require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', function (req, res) {
	res.send('Hello World!');
	res.end();
})

app.get('/testdb', function (req, res) {
	db.query("SELECT * FROM test", function(err, result){
		if(!err){
			res.json(result.rows);
		}
		else{
			res.send("Error a la DB");
			console.log(err);
		}
	});
})

app.get('/app', function(req, res){
	db.query("SELECT * FROM app", function(err, result){
		if(!err){
			res.json(result.rows);
		}
		else{
			res.send("Error a la DB");
			console.log(err);
		}
	});
})

app.get('/records', function(req, res){
	db.query("SELECT * FROM record", function(err, result){
		if(!err){
			res.json(result.rows);
		}
		else{
			res.send("Error a la DB");
			console.log(err);
		}
	});
})

app.get('/:app_code', function(req, res){
	db.query("SELECT * FROM record WHERE app_code ='" + req.params.app_code + "';", function(err, result){
		if(!err){
			res.json(result.rows);
		}
		else{
			res.send("ERROR");
			console.log(err);
		}
	});
})

app.post('/app/:app_code', function (req, res) {
db.query("INSERT INTO record(app_code,player,score) VALUES ($1,$2,$3);",[req.params.app_code,req.body.player,req.body.score],function(err,result){
	if(!err){
		res.send("POST OK");
	} else {
		res.status(500);
		res.end(err);
		console.log(err);
	}
});
})

app.get('/delete/:id', function(req, res){
	db.query("DELETE FROM record WHERE id = '" + req.params.id + "';", function(err, result){
	if(!err){
		res.end("DELETE OK");
	}else{
		res.end("ERROR");
	}
	});
})

app.listen(process.env.PORT || 3000, function () {
console.log('Example app listening on port 3000!')
})